package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static model.DataSource.*;
import static model.ConnectingToDB.connection;

public class Statements {
//    public static void insertInPersonsTable(Statement statement, String cnp, String name, int age) throws SQLException {
//        statement.execute("INSERT INTO" + DataSource.TABLE_PERSONS +
//                " ( " + DataSource.COLUMN_CNP,
//                DataSource.COLUMN_NAME,
//                DataSource.COLUMN_AGE + " ) "
//                + "VALUES(' " + cnp + " ', ' " + name + " '," + age+ ")" );
//
//    }
/*Nu vrem ca, clasele care acceseaza metodele din pachetul model sa cunoasca si implementarile care au loc in DB.
  De accea metodele noastre nu trebuie sa intoarca un ResultSet si ca alternativa, v-om reintoarce informatiile  folosindu-ne de
  o lista de tip Persons*/

     public List<Persons> queryAllPersonsOrderedBy(OrderOfPersons sortOrder) {
        List<Persons> persons = new ArrayList<>();

        StringBuilder sb = new StringBuilder("SELECT * FROM ");
        sb.append(TABLE_PERSONS + " ");

        // am folosit enum ca sa nu mearga sa introduc ca parametru altceva. Vrem sa dam ca parametrii ceva specific.
        // dorim sa nu avem valori invalide transmise ca parametru.

        if (sortOrder == OrderOfPersons.ORDER_BY_NONE) {
            sb.append("");
        } else if (sortOrder == OrderOfPersons.ORDER_BY_ASC) {
            sb.append(" ORDERED BY ");
            sb.append(OrderOfPersons.ORDER_BY_ASC.toString());
        } else if (sortOrder == OrderOfPersons.ORDER_BY_DESC) {
            sb.append(" ORDERED BY ");
            sb.append(OrderOfPersons.ORDER_BY_DESC.toString());
        }
        try (Statement statement = connection.createStatement();
             ResultSet results = statement.executeQuery(sb.toString())) {
            while (results.next()) {
                Persons person = new Persons(results.getString(INDEX_PERSON_CNP), results.getString(INDEX_PERSON_NAME), results.getInt(INDEX_PERSON_AGE));
                persons.add(person);
            }
            return persons;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
// aici este o metoda care intoarce  toate  cnp urile din tabela persons, va trenui sa schimb parametru cnp ,
//pt ca in acest caz o sa caut un singur cnp, si vreau sa imi returneze persona pe baza cnp ului. o sa las
//metoda asa pt ca mai tarziu, cand o sa am mai multe tabele , o sa fac cu aceasta metoda un join.
//dupa ce o sa bag mai mlte tabele in loc de cnp ca argument o sa bag o persoana(sau o farmacie) si o sa vreu sa
//imi returneze un query join cu alte tabele , spre exemplu vreu sa gasesc un medicament al unei farmacii anume

    public List<String> gueryCNPsFromPersons(String cnp, OrderOfPersons sortOrder) {
        // SELECT cnp * FROM persons WHERE cnp = cnp
        StringBuilder sb = new StringBuilder(QUERY_PERSONS_GET_CNP);
        sb.append(cnp);
        sb.append("\"");

        if (sortOrder == OrderOfPersons.ORDER_BY_NONE) {
            sb.append(QUERY_PERSONS_GET_CNP_ORDERED_BY_NONE);
        } else if (sortOrder == OrderOfPersons.ORDER_BY_ASC) {
            sb.append(" ORDERED BY ");
            sb.append(OrderOfPersons.ORDER_BY_ASC.toString());
        } else if (sortOrder == OrderOfPersons.ORDER_BY_DESC) {
            sb.append(" ORDERED BY ");
            sb.append(OrderOfPersons.ORDER_BY_DESC.toString());
        }
// aici printam sb ul ca sa vedem daca query ul(SQL statement) nostru este corect, verificam sa nu avem lipsa un spatiu sau altceva
        System.out.println(sb.toString());

        try (Statement statement = connection.createStatement();
             ResultSet results = statement.executeQuery(sb.toString())) {
            List<String> cnps = new ArrayList<>();
// aici noi practic reintoarcem toate cnp urile sortate dupa o anumita ordine
//sa nu uiti de acest scop si sa schimbi argumentul cnp dat metodei
            while (results.next()) {
                cnps.add(results.getString(INDEX_PERSON_CNP));// tine minte ca aici COLUMN_NAME reintoarce nr coloanei
                                                               //din result set, dar coincide pt ca coloana cnp este la pozitia 1
            }
            return  cnps;
        } catch (SQLException e) {
            System.out.println("Qerry from gueryCNPsFromPersons() faild: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }


}
