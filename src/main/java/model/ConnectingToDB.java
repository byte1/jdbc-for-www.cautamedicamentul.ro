package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static model.DataSource.*;


public class ConnectingToDB {
    static Connection connection;


    // we create 1 methods that open and close the DB
    // am lasat doar metoda open connection deoarce am folosit try with resources si o sa mi se inchida automat
    // deci nu mai avem nevoie de metoda closeConnectionToDB?!?

    public  boolean openConnectionToDatabase() {
        try (Connection connectionFromOpenDB = connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
            return true;
        } catch (SQLException e) {
            System.out.println("Couldn't connect to database " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

}

