package model;

public class DataSource {

    public static final String USER = "root";
    public static final String PASSWORD = "0241665441Gb:";
    public static final String DATABASE = "curs";
    public static final String URL = String.format("jdbc:mysql://localhost:3306/%s", DATABASE);

    public static final String TABLE_PERSONS = "persons";
    public static final String COLUMN_CNP = "cnp";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_AGE = "age";

// am declarat constantele de mai jos pt ca vreu sa folosesc int uri in cazul indecsilor coloanelor tabelei pt ca este o metoda mai rapida

    public static final int INDEX_PERSON_CNP = 1;
    public static final int INDEX_PERSON_NAME = 2;
    public static final int INDEX_PERSON_AGE = 3;

//creem query uri constante ca sa nu le scriem de fiecare data in program, primul
// query o sa fie pt metoda care afiseaza toata tabela iar al 2 lea query o sa fie pt
// afisarea coloanei de cnp uri.

    public static final String QUERY_ALL_PERSONS_ORDERED_BY = "SELECT * FROM " + TABLE_PERSONS + " ";
    //punem " la ultimul caracter pt ca sunt ghilimelele care cuprind cnp ul si nu ghilimelele prin care se declara query ul
//evident ca in metoda, dupa ce o sa inseram cnp ul o sa inchidem acete gilimele la fel cu: \"
    public static final String QUERY_PERSONS_GET_CNP = "SELECT FROM " + TABLE_PERSONS + " WHERE " + COLUMN_CNP + " = \"";
    public static final String QUERY_PERSONS_GET_CNP_ORDERED_BY_NONE = "ORDERED BY COLATE NOCASE";

    public enum OrderOfPersons {
        ORDER_BY_NONE("NONE"),
        ORDER_BY_ASC("ASC"),
        ORDER_BY_DESC("DESC");

        String orderSort;

        OrderOfPersons(String orderSort) {
            this.orderSort = orderSort;
        }

        @Override
        public String toString() {
            return orderSort;
        }
    }

    private DataSource() {
        throw new IllegalAccessError("Utility class, do not instantiate!");
    }
}
