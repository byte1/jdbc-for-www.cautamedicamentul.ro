package model;

public class Persons {
    private String name;
    private int age;
    private String cnp;

    public Persons() {
    }

    public Persons(String name, String cnp, int age) {
        this.name = name;
        this.age = age;
        this.cnp = cnp;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCnp() {
        return cnp;
    }

    @Override
    public String toString() {
        return "Persons{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", cnp='" + cnp + '\'' +
                '}';
    }
}
