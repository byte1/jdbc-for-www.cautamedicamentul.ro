import model.ConnectingToDB;
import model.DataSource;
import model.Persons;
import model.Statements;

import java.util.List;

//import static model.Statements.queryAllPersonsOrderedBy;

public class Main {
    public static void main(String[] args) {
        Statements statements = new Statements();
        // test pt a vedea daca are loc conexiunea

        ConnectingToDB connectingToDB = new ConnectingToDB();
        if(!connectingToDB.openConnectionToDatabase()){
            System.out.println("Can't open datasource");
            return;
        }
//-------------------- end tets------------------------------------

// aici am apelat metoda queryAllPersonsOrderedBy si am hendluit faptul ca metoda poate reintoarce un null
        List<Persons> persons = statements.queryAllPersonsOrderedBy(DataSource.OrderOfPersons.ORDER_BY_DESC);
        if(persons == null){
            System.out.println("No persons!");
            return;
        }
        for(Persons person : persons){
            System.out.println("CNP" + person.getCnp() + "Name " + person.getName() + "Age " + person.getAge()  );
        }
//------------------------------ urmeaza metoda de cnp--------------------------------------------------------------
// tine minte ca aici o sa trebuiasca sa schimbi primul parametru string atunci cand o sa ai mai multe tabele
        List<String> cnps = statements.gueryCNPsFromPersons("19403244147", DataSource.OrderOfPersons.ORDER_BY_DESC);
        for(String cnp : cnps){
            System.out.println(cnp);
        }





        //connectingToDB.close() la sf punem asa ceva daca o sa facem conexiunea cu try normal si o sa ii creem metoda de open si close

    }
}
